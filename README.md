# ros2-docker

## Build docker image
```shell
cd docker
docker compose -f docker-compose.yaml build ros2
```

## Run docker image
```shell
cd docker
docker compose -f docker-compose.yaml up ros2 -d
```

## Attach to docker container
```shell
docker exec -it ros2-devel zsh
```

## Stop container
```shell
docker stop ros2-devel 
```

## Start container 
```shell
docker start ros2-devel
```

### [Set up this configuration for MacOs](https://gist.github.com/vfdev-5/b7685371071036cb739f23b3794b5b83)